-- set LUA_PATH and LUA_CPATH to load also local modules
local prefix_path = './lua';
package.path = ('%s/?.lua;%s/?/init.lua;%s'):format(prefix_path, prefix_path, package.path)
package.cpath = ('%s/?.so;%s/?/init.so;%s'):format(prefix_path, prefix_path, package.cpath)

-- Initialize UCI config
local uci = require("uci")
--uci.set_confdir("/tmp/uci")
uci.set("lua", "sms", "lua")
uci.set("lua", "sms", "recaptcha_secret", 'disable-disable-disable-disable-disable-')
uci.set("lua", "sms", "admin_phone", '420123456789')
uci.set("lua", "sms", "phones", { 'test1:420111111111', 'test2:420222222222' })
uci.set("lua", "sms", "error_url", '/sms/error-parameters.html')
uci.set("lua", "sms", "saved_url", '/sms/ok-saved.html')
uci.set("lua", "sms", "recaptcha_url", '/sms/error-recaptcha.html')
uci.set("lua", "sms", "status_file", '/tmp/sms/stats/status')
uci.set("lua", "sms", "checked_dir", '/tmp/sms/checked')
uci.set("lua", "sms", "outgoing_dir", '/tmp/sms/outgoing')
uci.set("lua", "sms", "sent_dir", '/tmp/sms/sent')
uci.set("lua", "sms", "report_dir", '/tmp/sms/report')
uci.set("lua", "sms", "failed_dir", '/tmp/sms/failed')
uci.commit("lua")

require("uhttpd-lua-handler")

--function handle_request(env)
--	module ",  require("http-processors.sms")
--	module.process("sms", env)
--end
