#!/bin/sh

UCI_DIR=/etc/config

if ! [ -d "${UCI_DIR}" ]; then
	mkdir -p ${UCI_DIR}
	chmod 1777 ${UCI_DIR}
fi

# -- Initialize UCI config

touch ${UCI_DIR}/lua

uci -c ${UCI_DIR} set lua.sms=lua
uci -c ${UCI_DIR} set lua.sms.recaptcha_secret='disable-disable-disable-disable-disable-'
uci -c ${UCI_DIR} set lua.sms.admin_phone='420123456789'
uci -c ${UCI_DIR} add_list lua.sms.phones 'test1:420111111111'
uci -c ${UCI_DIR} add_list lua.sms.phones 'test2:420222222222'
uci -c ${UCI_DIR} set lua.sms.error_url='/sms/error-parameters.html'
uci -c ${UCI_DIR} set lua.sms.saved_url='/sms/ok-saved.html'
uci -c ${UCI_DIR} set lua.sms.recaptcha_url='/sms/error-recaptcha.html'
uci -c ${UCI_DIR} set lua.sms.status_file='/tmp/sms/stats/status'
uci -c ${UCI_DIR} set lua.sms.checked_dir='/tmp/sms/checked'
uci -c ${UCI_DIR} set lua.sms.outgoing_dir='/tmp/sms/outgoing'
uci -c ${UCI_DIR} set lua.sms.sent_dir='/tmp/sms/sent'
uci -c ${UCI_DIR} set lua.sms.report_dir='/tmp/sms/report'
uci -c ${UCI_DIR} set lua.sms.failed_dir='/tmp/sms/failed'
uci -c ${UCI_DIR} commit
