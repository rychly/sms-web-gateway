local _M = {}

local uhttpd_request = require("uhttpd-request")
local recaptcha_siteverify = require("recaptcha-siteverify")
local lfs = require("lfs")
local uci = require("uci")

local cfg = uci.cursor()
local phones_cfg = cfg:get("lua", "sms", "phones")
local phones = { }
for i,v in ipairs(phones_cfg) do
	sep_idx = v:find(':', 1, true)
	phones[v:sub(1, sep_idx-1)] = v:sub(sep_idx+1)
end

function _M.process(cmd, env)
	if (env.QUERY_STRING == "check") then
		_M.process_check(cmd, env)
	else
		_M.process_send(cmd, env)
	end
end

function _M.process_send(cmd, env)
	uhttpd.send("Status: 302 Found\r\n")
	local request_post = uhttpd_request.post_items(env)
	local recaptcha_secret = cfg:get("lua", "sms", "recaptcha_secret")
	local recaptcha_result, recaptcha_status = recaptcha_siteverify.verify(
		recaptcha_secret, request_post["g-recaptcha-response"] or "", env.REMOTE_ADDR)
	if recaptcha_result or (recaptcha_secret == 'disable-disable-disable-disable-disable-') then
		local phone = phones[request_post["recipient"]] or nil
		local message = request_post["message"] and string.gsub(request_post["message"], "^%s*(.-)%s*$", "%1") or nil
		if (phone == nil) or (message == nil) or (string.len(message) == 0) or (string.len(message) == 1024) then
			uhttpd.send(
				"Location: " .. cfg:get("lua", "sms", "error_url") .. "?"
				.. (phone == nil and "phone-error" or "phone-ok")
				.. "&"
				.. (message == nil or string.len(message) == 0 and "message-error" or "message-ok")
				.. "\r\n\r\n"
			)
			return
		end
		local filename = "web." .. env.REMOTE_ADDR:gsub("[.:]", "_") .. "." .. os.date("%Y-%m-%d-%H-%M-%S") .. '.' .. phone
		local file = assert(io.open(cfg:get("lua", "sms", "outgoing_dir") .. "/" .. filename, "w"))
		io.output(file)
		io.write(
			"To: " .. phone .. "\n" ..
			"Alphabet: UTF-8\n" ..
			"Report: yes\n" ..
			"\n" ..
			message
		)
		io.close(file)
		uhttpd.send("Location: " .. cfg:get("lua", "sms", "saved_url") .. "\r\n\r\n")
	else
		uhttpd.send(
			"Location: " .. cfg:get("lua", "sms", "recaptcha_url") .. "?"
			.. table.concat(recaptcha_status, "&")
			.. "\r\n\r\n"
		)
	end
end

function _M.add_msgs_status(output_table, directory, client_id, status)
	for file in assert(lfs.dir(directory)) do
		-- format = web.sessionId.Y-m-d-H-i-s.phoneNumber.messageNumber
		-- e.g., web.c2b3c4f49a6c92a4ac1992056c2ea815.2016-03-09-15-41-25.420111111111.126
		for file_client_id, file_date, file_phone, file_others in file:gmatch("web\.([^.]*)\.([^.]*)\.([^.]*)\.?(.*)$") do
			if ((file_others:sub(-5) ~= "LOCK") and ((client_id == nil) or (client_id == file_client_id))) then
				table.insert(output_table, { file_date, file_phone, status:format(file_others) })
			end
		end
	end
end

function _M.table_invert(input_table)
	local output_table = {}
	for k, v in pairs(input_table) do
		output_table[v] = k
	end
	return output_table
end

function _M.parse_date_to_time(date_string)
	local parsed_date = date_string:gmatch("[^-]+")
	local date_time_table = {
		["year"] = parsed_date(),
		["month"] = parsed_date(),
		["day"] = parsed_date(),
		["hour"] = parsed_date(),
		["min"] = parsed_date(),
		["sec"] = parsed_date()
	}
	return os.time(date_time_table)
end

function _M.process_check(cmd, env)
	local file = assert(io.open(cfg:get("lua", "sms", "status_file"), "r"))
	file:read("*l")	-- skip the first line
	while true do
		local line = file:read("*l")
		if (line == nil) then
			break
		else
			local cols = line:gmatch("[^:,\t]+")
			uhttpd.send("Status: 200 OK\r\n")
			uhttpd.send("Content-Type: text/html\r\n\r\n")
			local modem, date, mode, sent, failed, received, signal_type, signal = cols(), cols(), cols(), cols(), cols(), cols(), cols(), cols()
			uhttpd.send('<dl id="modemStatus" title="' .. modem .. '">\n'
				.. '<dt>Date</td><dd>' .. os.date("%Y-%m-%d %H:%M:%S", _M.parse_date_to_time(date)) .. '</dd>\n'
				.. '<dt>Status</dt><dd>' .. mode .. '</dd>\n'
				.. '<dt>Sent</dt><dd>' .. sent .. '</dd>\n'
				.. '<dt>Failed</dt><dd>' .. failed .. '</dd>\n'
				.. '<dt>Received</dt><dd>' .. received .. '</dd>\n'
				.. '<dt>Signal</dt><dd title="' .. (signal_type or 'N/A')..'">' .. (signal or 'N/A') .. '</dd>\n'
				.. '</dl>\n')
		end
	end
	io.close(file)
	local msgs_status, phones_inverted = {}, _M.table_invert(phones)
	local my_session = env.REMOTE_ADDR:gsub("[.:]", "_")
	_M.add_msgs_status(msgs_status, cfg:get("lua", "sms", "outgoing_dir"), nil, "outgoing")
	_M.add_msgs_status(msgs_status, cfg:get("lua", "sms", "checked_dir"), nil, "checked")
	_M.add_msgs_status(msgs_status, cfg:get("lua", "sms", "sent_dir"), nil, "sent (%s)")
	_M.add_msgs_status(msgs_status, cfg:get("lua", "sms", "report_dir"), nil, "reported (%s)")
	_M.add_msgs_status(msgs_status, cfg:get("lua", "sms", "failed_dir"), nil, "failed")
	table.sort(msgs_status, function (v1, v2) return v1[1] > v2[1] end)
	uhttpd.send('<table id="messageStatus">\n'
		.. '<tr><th>Date</th><th>Phone</th><th>Status</th></tr>\n')
	for _, msg_status in ipairs(msgs_status) do
			uhttpd.send("<tr><td>"
				.. os.date("%Y-%m-%d %H:%M:%S", _M.parse_date_to_time(msg_status[1]))
				.. "</td><td>"
				.. (phones_inverted[msg_status[2]] or "unknown")
				.. "</td><td>"
				.. msg_status[3]
				.. "</td></tr>\n")
	end
	uhttpd.send('</table>\n')
end

return _M
