# SMS Web Gateway

Web gateway for sending SMS via SMS Server Tools 3 (smstools3).

## Usage

For usage, see [run-uhttpd.sh](./run-uhttpd.sh) and [lua/http-processors/sms.lua](./lua/http-processors/sms.lua).
